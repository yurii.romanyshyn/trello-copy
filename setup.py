import setuptools


setuptools.setup(
    name="trello",
    version="0.0.1",
    author="Yurii Romanyshyn",
    author_email="yurii.romanyshyn@starnavi.io",
    description="trello",
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires='>=3.6'
)
