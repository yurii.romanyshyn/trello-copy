import os
from functools import partial
from datetime import datetime
from sqlalchemy import event

from trello.extensions import db
from trello.utils import slugify
from trello.utils import generate_token
from trello.security.passwords import validate_password
from trello.security.passwords import make_password
from trello.security.passwords import check_password



class CRUDMixin(object):

    @classmethod
    def exists(cls, **lookup):
        exists_query = cls.query.filter_by(**lookup).exists()
        return db.session.query(exists_query).scalar()
    
    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        instance.save()
        return instance

    def update(self, commit=True, **kwargs):
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        if commit: self.save()

    def save(self, commit=True):
        db.session.add(self)
        if commit: 
            db.session.commit()

    def delete(self, commit=True):
        db.session.delete(self)
        if commit: 
            db.session.commit()



class BaseModel(db.Model, CRUDMixin):
    __abstract__ = True


class User(BaseModel):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), nullable=False, unique=True)
    email = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(100), nullable=False)
    activation_code = db.Column(db.String(100), default=generate_token)
    is_active = db.Column(db.Boolean, nullable=False, default=False)
    is_staff = db.Column(db.Boolean, nullable=False, default=False)
    is_superuser = db.Column(db.Boolean, nullable=False, default=False)
    joined_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    activated_at = db.Column(db.DateTime)

    tokens = db.relationship(
        "TokenCredentials", 
        back_populates="user",
        cascade="all, delete, delete-orphan"
    )
    profile = db.relationship(
        "UserProfile", 
        back_populates="user", 
        uselist=False,
        cascade="all, delete, delete-orphan"
    )
    boards = db.relationship(
        "Board", 
        back_populates="owner",
        cascade="all, delete, delete-orphan"
    )
    scopes = db.relationship(
        "Scope", 
        back_populates="owner",
        cascade="all, delete, delete-orphan"
    )
    tasks = db.relationship(
        "Task", 
        back_populates="owner",
        cascade="all, delete, delete-orphan"
    )

    @classmethod
    def create(cls, username, email, raw_password, profile_data=None):
        profile_data = profile_data or {}
        instance = cls(username=username, email=email)
        instance.set_password(raw_password)
        instance.profile = UserProfile(**profile_data)
        instance.save()
        return instance

    def set_password(self, raw_password):
        self.password = make_password(validate_password(raw_password))

    def check_password(self, raw_password):
        return check_password(self.password, raw_password)

    def activate_account(self):
        self.is_active = True
        self.activation_code = None
        self.activated_at = datetime.now()


class TokenCredentials(BaseModel):
    __tablename__ = "token_credentials"

    id = db.Column(db.Integer, primary_key=True)
    access_token = db.Column(
        db.String(40),
        unique=True, 
        nullable=False, 
        default=generate_token
    )
    refresh_token = db.Column(
        db.String(40),
        unique=True,
        nullable=False,
        default=generate_token
    )
    access_token_expires_after = db.Column(db.DateTime, nullable=False)
    refresh_token_expires_after = db.Column(db.DateTime, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)

    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    user = db.relationship("User", back_populates="tokens")


class UserProfile(BaseModel):
    __tablename__ = "user_profiles"

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    city = db.Column(db.String(255))
    country = db.Column(db.String(255))
    company = db.Column(db.String(255))
    birthday = db.Column(db.Date)
    bio = db.Column(db.Text)

    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    user = db.relationship("User", back_populates="profile")


class Board(BaseModel):
    __tablename__ = "boards"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False, unique=True)
    description = db.Column(db.Text)
    background_color = db.Column(db.String(7), nullable=False, default="#ffffff")
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)

    owner_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    owner = db.relationship("User", back_populates="boards")
    
    scopes = db.relationship(
        "Scope", 
        back_populates="board",
        cascade="all, delete, delete-orphan"
    )


class Scope(BaseModel):
    __tablename__ = "scopes"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False, unique=True)
    position = db.Column(db.Integer, nullable=False)
    description = db.Column(db.Text)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)

    board_id = db.Column(db.Integer, db.ForeignKey("boards.id"))
    board = db.relationship("Board", back_populates="scopes")

    owner_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    owner = db.relationship("User", back_populates="scopes")

    tasks = db.relationship(
        "Task", 
        back_populates="scope",
        cascade="all, delete, delete-orphan"
    )


task_label_association = db.Table(
    "tasks_labels_association",
    db.Column("task_id", db.Integer, db.ForeignKey("tasks.id")),
    db.Column("comments_id", db.Integer, db.ForeignKey("comments.id"))
)


class Task(BaseModel):
    __tablename__ = "tasks"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False, unique=True)
    position = db.Column(db.Integer, nullable=False)
    description = db.Column(db.Text)
    duedate = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)

    scope_id = db.Column(db.Integer, db.ForeignKey("scopes.id"))
    scope = db.relationship("Scope", back_populates="tasks")

    owner_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    owner = db.relationship("User", back_populates="tasks")

    notes = db.relationship(
        "Note", 
        back_populates="task",
        cascade="all, delete, delete-orphan"
    )
    labels = db.relationship("Label", secondary=task_label_association)


class Label(BaseModel):
    __tablename__ = "comments"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False, unique=True)
    color = db.Column(db.String(7), nullable=False, default="#ffffff")
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)


class Note(BaseModel):
    __tablename__ = "notes"

    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    
    task_id = db.Column(db.Integer, db.ForeignKey("tasks.id"))
    task = db.relationship("Task", back_populates="notes")


# SqlAlchemy event listeners

def similar_slug_exists(model, slug_value, slug_field_name="slug"):
    exists_query = model.query.filter_by(**{slug_field_name: slug_value}).exists()
    return model.session.query(exists_query).scalar()


def generate_slug_field_event(target, value, old_value, initiator):
    target.slug = slugify(value, partial(similar_slug_exists, target))


event.listen(Board.title, "set", generate_slug_field_event)
event.listen(Scope.title, "set", generate_slug_field_event)
event.listen(Task.title, "set", generate_slug_field_event)