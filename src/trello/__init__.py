import json
from flask import Flask


def create_application():
    app = Flask("trello")
    app.config.from_envvar("FLASK_SETTINGS")
    register_extensions(app)
    register_blueprints(app)
    register_error_hadlers(app)
    return app


def register_extensions(app):
    from trello.extensions import (
        cors, db, migrate, dramatiq, mail
    )
    cors.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db, app.config["MIGRATIONS_DIRECTORY"])
    dramatiq.init_app(app)
    mail.init_app(app)


def register_blueprints(app):
    from trello.blueprints.auth import auth_blueprint
    app.register_blueprint(auth_blueprint)


def register_error_hadlers(app):
    
    def api_error_hamdler(error):
        return error.as_dict(), error.status_code
    
    from trello.errors import APIError
    app.register_error_handler(APIError, api_error_hamdler)