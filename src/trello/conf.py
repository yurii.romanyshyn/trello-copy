from functools import lru_cache
from flask import current_app as app
from pydantic import ValidationError

from trello.errors import ImproperlyConfiguredError


@lru_cache(maxsize=None)
def get_settings_key(key, default=None):
    return app.config.get(key, default)


@lru_cache(maxsize=None)
def get_policy(key, policy_model_class):
    policy_config = app.config.get(key, {})
    try:
        return policy_model_class(**policy_config)
    except ValidationError as error:
        raise ImproperlyConfiguredError(
            f"Improperly configured policy - {key}:\n"
            f"{error}\n"
        )