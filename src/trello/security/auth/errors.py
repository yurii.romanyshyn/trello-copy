from trello.errors import BaseError


class AuthenticationError(BaseError):
    pass


class InvalidPasswordError(AuthenticationError):
    message = "Invalid password"
    