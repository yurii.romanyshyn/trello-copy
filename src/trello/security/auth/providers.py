from datetime import datetime
from datetime import timedelta
from .errors import InvalidPasswordError


class BaseAuthenticationProvider:

    def __init__(self, flask_app, user_model):
        self.app = flask_app
        self.user_model = user_model

    def authenticate(self, email, raw_password):
        user = self.user_model.query.filter_by(email=email).first()
        if not user: return None
        is_password_valid = user.check_password(raw_password)
        if not is_password_valid: raise InvalidPasswordError()
        return user

    def login(self, request, user):
        raise NotImplementedError

    def logout(self, request, user):
        raise NotImplementedError

    def login_required(self, view_func):
        raise NotImplementedError



class TokenAuthenticationProvider:
    ACCESS_TOKEN_TTL_CONFIG_KEY = "ACCESS_TOKEN_TTL" 
    REFRESH_TOKEN_TTL_CONFIG_KEY = "REFRESH_TOKEN_TTL"

    default_access_token_ttl = 3600 # seconds, 1 hour
    default_refresh_token_ttl = 7889400 # seconds, 3 month

    def __init__(self, flask_app, token_model):
        self.app = flask_app
        self.token_model = token_model

    def authenticate(self, token):
        pass

    def generate_token(self, user):
        now = datetime.now()
        new_token = self.token_model(
            user=user,
            access_token_expires_after=(
                now + timedelta(seconds=self.get_access_token_ttl())
            ),
            refresh_token_expires_after=(
                now + timedelta(seconds=self.get_refresh_token_ttl())
            ),
        )
        new_token.save()
        return new_token

    def refresh_token(self):
        pass

    def revoke_token(self):
        pass

    def token_required(self, view_func):
        pass

    def get_access_token_ttl(self):
        return (
            self.app.config.get(self.ACCESS_TOKEN_TTL_CONFIG_KEY) 
            or self.default_access_token_ttl
        )

    def get_refresh_token_ttl(self):
        return (
            self.app.config.get(self.REFRESH_TOKEN_TTL_CONFIG_KEY) 
            or self.default_refresh_token_ttl
        )