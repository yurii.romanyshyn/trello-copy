from operator import imod
from flask import current_app
from trello.models import User
from trello.models import TokenCredentials
from .errors import InvalidPasswordError
from .providers import BaseAuthenticationProvider
from .providers import TokenAuthenticationProvider


base_auth_provider = BaseAuthenticationProvider(current_app, User)
token_auth_provider = TokenAuthenticationProvider(current_app, TokenCredentials)