from trello.errors import BaseError


class PasswordValidationError(BaseError):
    pass
    