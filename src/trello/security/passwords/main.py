import typing
from functools import lru_cache
from werkzeug.utils import import_string
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash
from pydantic import BaseModel as ConfigModel

from trello.conf import get_policy
from trello.errors import ImproperlyConfiguredError
from .errors import PasswordValidationError


__all__ = ["validate_password", "make_password", "check_password"]


PASSWORD_POLICY_KEY = "PASSWORD_POLICY"


class HashingOptions(ConfigModel):
    salt_length: int = 16
    method: str = None

    class Config:
        alias_generator = lambda s: s.upper()


class ValidatorSetting(ConfigModel):
    path: str
    options: typing.Dict[str, typing.Any] = {}
    
    class Config:
        alias_generator = lambda s: s.upper()


class PasswordPolicy(ConfigModel):
    validators: typing.List[ValidatorSetting] = []
    hashing_options: HashingOptions = HashingOptions()

    class Config:
        alias_generator = lambda s: s.upper()


@lru_cache(maxsize=None)
def get_password_validators():
    policy = get_policy(PASSWORD_POLICY_KEY, PasswordPolicy)
    unique_validators = {
        validator.path: validator.options 
        for validator in policy.validators
    }
    return [
        import_string(path)(**options)
        for path, options in unique_validators.items()
    ]
    


def validate_password(password):
    validators = get_password_validators()

    if not validators:
        raise ImproperlyConfiguredError(
            f"Password validators are not specified"
        )

    errors = []
    
    for validator in validators:
        assert callable(validator), "Password validator must be a callable"
        
        try:
            validator(password)
        except PasswordValidationError as error:
            errors.append(error.message)
    
    if errors:
        raise PasswordValidationError(
            "Unvalid password",
            errors=errors
        )
    
    return password


def make_password(password, **kwargs):
    policy = get_policy(PASSWORD_POLICY_KEY, PasswordPolicy)
    policy_salt_len = policy.hashing_options.salt_length
    policy_hashing_method = policy.hashing_options.method
    
    if policy_hashing_method is not None:
        kwargs.setdefault("method", policy_hashing_method)

    if policy_salt_len is not None:
        kwargs.setdefault("salt_length", policy_salt_len)
    
    return generate_password_hash(password, **kwargs)


def check_password(hashed_password, password):
    return check_password_hash(hashed_password, password)