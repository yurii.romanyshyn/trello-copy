import gzip
from .errors import PasswordValidationError


class MinimumLengthValidator:
    minimum_length = 6
    error_message = (
        "the password is too short. It must contain "
        "at least {minimum_length} characters."
    )

    def __init__(self, **kwargs):
        self.minimum_length = kwargs.get("minimum_length") or self.minimum_length

    def __call__(self, password, **kwargs):
        if len(password) < self.minimum_length:
            raise PasswordValidationError(self.get_error_message())

    def get_error_message(self):
        return self.error_message.format(
            minimum_length=self.minimum_length
        )


class CommonPasswordValidator:
    """
    Validate whether the password is a common password.

    The password is rejected if it occurs in a provided list of passwords,
    which may be gzipped. The list Django ships with contains 20000 common
    passwords (lowercased and deduplicated), created by Royce Williams:
    https://gist.github.com/roycewilliams/281ce539915a947a23db17137d91aeb7
    The password list must be lowercased to match the comparison in validate().
    """
    error_message = "Password is too common"

    def __init__(self, common_password_path, **kwargs):
        self.common_passwords_path = common_password_path
        self.common_passwords = self.load_common_passwords(common_password_path)
    
    def load_common_passwords(self, path):
        try:
            with gzip.open(path, 'rt', encoding='utf-8') as f:
                return {x.strip() for x in f}
        except OSError:
            with open(path) as f:
                return {x.strip() for x in f}

    def __call__(self, password, **kwargs):
        if password.lower().strip() in self.common_passwords:
            raise PasswordValidationError(self.get_error_message())

    def get_error_message(self):
        return self.error_message


class NumericPasswordValidator:
    error_message = "Password can’t be entirely numeric."

    def __init__(self, **kwargs):
        pass
    
    def __call__(self, password, **kwargs):
        if password.isdigit():
            raise PasswordValidationError(self.get_error_message())

    def get_error_message(self):
        return self.error_message