import os
from binascii import hexlify
from random import choice
from string import ascii_lowercase
from string import digits
from slugify import slugify
from pydantic import ValidationError

from .errors import InvalidInputError


def random_string_generator(size=10, chars=ascii_lowercase + digits):
    return ''.join(choice(chars) for _ in range(size))


def generate_token():
    return hexlify(os.urandom(20)).decode()


def slugify(value, slug_exists_func):
    original_slug = slugify(value)
    slug = original_slug
    
    while True:
        if slug_exists_func(slug):
            random_string = random_string_generator()
            slug = f"{original_slug}-{random_string}"
            continue
        else:
            return slug


def serialize_or_error(data, schema, error_class=None):
    try:
        return schema(**data)
    except ValidationError as error:
        if error_class:
            raise error_class(errors=error.errors()) from None
        else:
            raise InvalidInputError(errors=error.errors()) from None