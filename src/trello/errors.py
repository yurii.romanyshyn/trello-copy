

class BaseError(Exception):
    _representation_template = "<{error_name}:\n{error_content}\n>"

    def __init__(self, message=None, **kwargs):
        self.message = message or getattr(self, "message", None)
        
        if not self.message:
            raise ValueError("Error instance must contain error message")
        
        self.__dict__.update(kwargs)
        super().__init__(message)

    def get_error_content(self):
        for attr_name in dir(self):
            attr_value = getattr(self, attr_name)
            if (
                not attr_name.startswith("_")
                and not callable(attr_value)
                and attr_name != "args"  # attribute of Exception instance
            ):
                yield attr_name, attr_value

    def representation_kwargs(self):
        return {
            "error_name": self.__class__.__name__,
            "error_content": ";\n".join(
                f"{attr_name}: {attr_value}"
                for attr_name, attr_value in self.get_error_content()
            )
        }

    def __str__(self):
        return self._representation_template.format(
            **self.representation_kwargs()
        )


class ImproperlyConfiguredError(BaseError):
    pass


class APIError(BaseError):
    status_code = None
    code = None
    details = None
    errors = None
    
    def as_dict(self):
        return {
            "code": self.code,
            "details": self.details or self.message,
            "errors": self.errors
        }


class ResourceNotFoundError(APIError):
    message = "Requested resource not found"
    status_code = 404
    code = "resource-not-found"


class AccessForbiddenError(APIError):
    message = "Access Forbidden"
    status_code = 403


class InvalidInputError(APIError):
    status_code = 400
    message = "Received invalid input from user"
    code = "invalid-input"
    details = "Invalid input"