from flask import url_for
from flask_mail import Message

from trello.extensions import mail
from trello.extensions import dramatiq
from trello.models import User


@dramatiq.actor(max_retries=2)
def send_activation_mail(activation_link, user_email):
    message = Message(
        body=f"Welcome to Trello (copy),\nyour activation link {activation_link}",
        recipients=[user_email]
    )
    mail.send(message)
