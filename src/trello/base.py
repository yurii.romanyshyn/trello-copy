from flask_sqlalchemy import BaseQuery
from trello.errors import ResourceNotFoundError


class ExtendendQuery(BaseQuery):
    api_not_found_error = ResourceNotFoundError
    
    def get_or_api_404(self, ident, **error_kwargs):
        instance = self.get(ident)
        if instance is None:
            raise self.api_not_found_error(**error_kwargs)
        return instance

    def first_or_api_404(self, **error_kwargs):
        instance = self.first()
        if instance is None:
            raise self.api_not_found_error(**error_kwargs)
        return instance