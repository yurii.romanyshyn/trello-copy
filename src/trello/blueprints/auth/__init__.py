from flask import Blueprint
from .views import SigninView
from .views import SignupView
from .views import AccountActivationView


auth_blueprint = Blueprint("auth", __name__)

auth_blueprint.add_url_rule(
    "/api/signup/", 
    view_func=SignupView.as_view("signup-view")
)
auth_blueprint.add_url_rule(
    "/api/account/<user_id>/activate/<activation_code>/",
    view_func=AccountActivationView.as_view("activation-view")
)
auth_blueprint.add_url_rule(
    "/api/signin/",
    view_func=SigninView.as_view("signin-view")
)