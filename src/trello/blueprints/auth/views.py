from flask import request
from flask import url_for
from flask import current_app as app
from flask.views import View

from trello.models import User
from trello.utils import serialize_or_error
from trello.security.auth import base_auth_provider
from trello.security.auth import token_auth_provider
from trello.security.auth import InvalidPasswordError
from trello.security.passwords import PasswordValidationError
from trello.errors import APIError
from trello.errors import ResourceNotFoundError
from trello.errors import AccessForbiddenError
from trello.tasks import send_activation_mail
from .schemas import SignupInputSchema
from .schemas import SignupOutputSchema
from .schemas import SigninInputSchema
from .schemas import TokenCredentialsSchema



class SignupView(View):
    methods = ["POST"]
    input_schema = SignupInputSchema
    output_schema = SignupOutputSchema

    def dispatch_request(self, *args, **kwargs):
        input_data = self.load_input()
        user_exists_errors = []

        if User.exists(username=input_data.username):
            user_exists_errors.append(
                f"User with 'username' {input_data.username} already exist"
            )
        
        if User.exists(email=input_data.email):
            user_exists_errors.append(
                f"User with 'email' {input_data.email} already exist",
            )
            
        if user_exists_errors:
            raise APIError(
                "User already exists",
                status_code=409,
                code="user-exists",
                errors=user_exists_errors
            )

        try:
            user = User.create(
                username=input_data.username,
                email=input_data.email,
                raw_password=input_data.password.get_secret_value()
            )
        except PasswordValidationError as error:
            raise APIError(
                "Not strong enough password",
                code="invalid-password",
                errors=error.errors
            ) from None
        else:
            self.send_mail(user)
            return self.make_response(self.output_schema())

    def load_input(self, data=None, **kwargs):
        data = data or request.get_json(cache=True)
        return serialize_or_error(data, self.input_schema, **kwargs)

    def make_response(self, data=None, status=201, headers=None):
        return app.response_class(
            response=data,
            status=status,
            headers=headers,
            content_type="application/json"
        )

    def send_mail(self, user):
        send_activation_mail.send(
            self.make_activation_link(user),
            user.email
        )
    
    def make_activation_link(self, user):
        return url_for(
            'auth.activation-view',
            user_id=user.id,
            activation_code=user.activation_code,
            _external=True
        )



class AccountActivationView(View):
    methods = ["GET", "POST"]

    def dispatch_request(self, user_id, activation_code):
        user = (
            User.query.filter(
                (User.id==user_id) 
                & (User.activation_code==activation_code)
                & (User.is_active==False)
            ).first_or_404()
        )
        user.activate_account()
        user.save()
        return self.post_activate_action()
    
    def post_activate_action(self):
        return "<h2>Account activated</h2>", 200



class SigninView(View):
    methods = ["POST"]
    base_auth_provider = base_auth_provider
    token_auth_provider = token_auth_provider
    input_schema = SigninInputSchema
    output_schema = TokenCredentialsSchema

    def dispatch_request(self, *args, **kwargs):
        user_credentials = self.load_input()
        user = self.auth(user_credentials)
        access_token = self.token_auth_provider.generate_token(user)
        return self.make_response(self.output_schema.from_orm(access_token).json())

    def load_input(self, data=None, **kwargs):
        data = data or request.get_json(cache=True)
        return serialize_or_error(data, self.input_schema, **kwargs)

    def make_response(self, data=None, status=200, headers=None):
        return app.response_class(
            response=data,
            status=status,
            headers=headers,
            content_type="application/json"
        )

    def auth(self, user_credentials):
        try:
            user = self.base_auth_provider.authenticate(
                user_credentials.email,
                user_credentials.password.get_secret_value()
            )
        except InvalidPasswordError:
            raise AccessForbiddenError(
                message="Incorrect password was provided",
                code="incorrect-password"
            )
        else:
            if not user:
                raise ResourceNotFoundError(
                    message=f"User with email {user_credentials.email} does not exist"
                )
            return user