from datetime import datetime
from pydantic import BaseModel
from pydantic import EmailStr
from pydantic import SecretStr


class SignupInputSchema(BaseModel):
    username: str
    email: EmailStr
    password: SecretStr


class SignupOutputSchema(BaseModel):
    message: str = "ok"


class SigninInputSchema(BaseModel):
    email: EmailStr
    password: SecretStr


class TokenCredentialsSchema(BaseModel):
    access_token: str
    refresh_token: str
    access_token_expires_after: datetime
    refresh_token_expires_after: datetime

    class Config:
        orm_mode = True