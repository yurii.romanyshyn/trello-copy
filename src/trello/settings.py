import os
from pathlib import Path

PROJECT_PACKAGE_DIR = Path(__file__).parent
PROJECT_DIR = PROJECT_PACKAGE_DIR.parent.parent
RESOURCES_DIR = PROJECT_DIR / "res" / "common-passwords.txt.gz"

ENV = "development"
DEBUG = True
SECRET_KEY = os.environ["SECRET_KEY"]

MIGRATIONS_DIRECTORY = (PROJECT_PACKAGE_DIR  / "migrations").absolute()
SQLALCHEMY_DATABASE_URI = "sqlite:///./trello.sqlite3"
SQLALCHEMY_TRACK_MODIFICATIONS = False

ALEMBIC = {
    "script_location": "./migrations/"
}

DRAMATIQ_BROKER = "dramatiq.brokers.redis:RedisBroker"

MAIL_SERVER = "smtp.gmail.com"
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USERNAME = "yurii.romanyshyn@starnavi.io"
MAIL_PASSWORD = os.environ["MAIL_PASSWORD"]
MAIL_DEFAULT_SENDER = "yurii.romanyshyn@starnavi.io"

ACCOUNT_ACTIVATION_REDIRECT_URL = None

PASSWORD_POLICY = {
    "VALIDATORS": [
        {
            "PATH": "trello.security.passwords.validators.MinimumLengthValidator",
            "OPTIONS": {"minimum_length": 4}
        },
        {
            "PATH": "trello.security.passwords.validators.CommonPasswordValidator",
            "OPTIONS": {"common_password_path": RESOURCES_DIR.absolute()}
        },
        {
            "PATH": "trello.security.passwords.validators.NumericPasswordValidator"
        }
    ]
}