from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_dramatiq import Dramatiq
from flask_mail import Mail

from .base import ExtendendQuery


cors = CORS()
db = SQLAlchemy(query_class=ExtendendQuery)
migrate = Migrate()
dramatiq = Dramatiq()
mail = Mail()