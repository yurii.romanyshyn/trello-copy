import os
from dotenv import load_dotenv
os.environ.setdefault("FLASK_SETTINGS", "settings.py")
load_dotenv(verbose=True, override=True)

from trello import create_application
application = create_application()